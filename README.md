# Демо работы с Kinopoisk API

### Развертывание
Для развертывания положить в каталог со статическими файлами вебсервера. 
Можно просто скачать и открыть в браузере файл `index.html`

### Коллекция Postman
Коллекция для Postman находится в файле `Kinopiosk_API.postman_collection.json`

### Использованные ресурсы
- Kinopoisk API (api.kinopoisk.cf)
- Angular.js 1.5
- Bootstrap 3.3 
- Yandex.Maps API

### Странности
При запуске через LivePreview в Brackets запрос 
    `http://api.kinopoisk.cf/getTodayFilms?cityID=490` 
иногда выдает только два фильма при первом запуске. При выборе города или обновлении страницы все ОК. 
Видимо особенность Brackets. В других случаях не замечено.




