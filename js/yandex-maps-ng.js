angular.module('yandex-maps',[]).directive('yandexMap', function () {

    function link(scope, element, attrs) {

        if (element[0].id === '')
            element[0].id = 'ng-yandex-maps';

        console.log('NgYandexMaps: loading map into:', element[0].id);

        var map;
        var centerGeoObject;

        if( typeof ymaps === 'undefined' ){
            element[0].innerHTML="<b>Cannot find Yandex.Maps api. You should include in first on your page.</b>"
            throw new Error('Cannot find Yandex.Maps api. You should include in first on your page.')
        }

        ymaps.ready(init);

        function init() {
            if (attrs.address && attrs.address.length > 0) {
                console.log('NgYandexMaps: map with address: ', attrs.address);
                findCoords(attrs.address, function (geoObject) {
                    centerGeoObject = geoObject;
                    createMap(geoObject.geometry.getCoordinates());п
                    map.geoObjects.add(geoObject);
                });
            } else {
                console.log('NgYandexMaps: map without addres');
                createMap();
            }

            attrs.$observe('address', function (newAddr) {
                console.log('NgYandexMaps: address changed: ', newAddr);
                centerToAddress(newAddr);
            });

        };

        function createMap(center) {
            console.log('NgYandexMaps: creating map');
            map = new ymaps.Map(element[0].id, {
                center: center || attrs.center || [55.76, 37.64],
                zoom: attrs.zoom || 7,
                controls: attrs.controls ? attrs.controls.replace(/[\[\]]/gi, '').split(',') : []
            });
            window.map = map;
        }

        function findCoords(addr, cb) {
            console.log('NgYandexMaps: looking for address: ', attrs.address);
            var geocoder = ymaps.geocode(addr);
            geocoder.then(function (res) {
                var geoObject = res.geoObjects.get(0);
                console.log('NgYandexMaps: addres found:', geoObject.geometry.getCoordinates());
                cb(geoObject);
            });
            geocoder.catch(function (err) {
                console.warn('NgYandexMaps: address not found');
                throw new Error(err);
            });
        }

        function centerToAddress(addr, cb) {
            findCoords(addr, function (geoObject) {
                map.setCenter(geoObject.geometry.getCoordinates());
                if (centerGeoObject)
                    map.geoObjects.remove(centerGeoObject);
                centerGeoObject = geoObject;
                map.geoObjects.add(geoObject);
                cb(geoObject);
            })
        }

    }

    return {
        link: link
    };
})
