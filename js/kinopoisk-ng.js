var module = angular.module('kinopoisk', []);

module.service('kpApi', function ($http) {

    var Api = {};

    Api.cities = {
        "490": "Калининград",
        "1": "Москва",
        "2": "Санкт-Петербург"
    };

    Api.genres = [
        "Любой",
        "Триллер",
        "Ужасы"
    ];

    Api.currentCity = "490";
    Api.currentGenre = "Любой";
    Api.orderByRating = false;
    Api.currentAddress = "Санкт-Петербург";

    Api.mpaaDesc = {
        "G": "Нет возрастных ограничений",
        "PG": "Рекомендуется присутствие родителей",
        "PG-13": "Детям до 13 лет просмотр не желателен",
        "R": "Лицам до 17 лет обязательно присутствие взрослого",
        "NC-17": "Лицам до 18 лет просмотр запрещен",
        "X": "Лицам до 18 лет просмотр запрещен"
    };

    Api.films = [];

    Api.reloadFilms = function () {
        var city = Api.currentCity;
        Api.getTodayFilms(city)
            .then(function (films) {
                Api.films = films;
                var genresDict = {};
                var genres = ["Любой"];
                for (var i = 0; i < films.length; i++) {
                    garr = films[i].genre.split(", ");
                    for (var j = 0; j < garr.length; j++) {
                        if (!genresDict[garr[j]]) {
                            genres.push(garr[j]);
                            genresDict[garr[j]] = true;
                        }
                    }
                }
                Api.genres = genres;
            });
    };


    Api.getTodayFilms = function (cityID) {
        //        return $http.get('http://api.kinopoisk.cf/getTodayFilms?cityID=' + cityID)
        return $http.get('http://api.kinopoisk.cf/getTodayFilms', {
                params: {
                    cityID: cityID
                }
            })
            .then(function (resp) {
                console.log('---responce: ', resp.data.filmsData);
                var data = resp.data.filmsData;
                data.map(function (film) {
                    film._posterURL = film.posterURL;
                    film.posterURL = Api.getPoster(film);
                    film.rating = film.rating || "0";
                    film.genre = film.genre || "";

                    //fix empty rating for some films
                    if (film.rating === "0") {
                        Api.getFilm(film.id).then(function (res) {
                            film.rating = res.ratingData.rating;
                        });
                    }
                });
                return data;
            });
    };

    Api.getSeances = function (filmID, cityID) {
        return $http.get('http://api.kinopoisk.cf/getSeance', {
                params: {
                    filmID: filmID,
                    cityID: cityID
                }
            })
            .then(function (resp) {
                return resp.data;
            });
    };

    Api.getFilm = function (filmID) {
        return $http.get('http://api.kinopoisk.cf/getFilm', {
                params: {
                    filmID: filmID
                }
            })
            .then(function (resp) {
                var film = resp.data;
                console.log('---film received: ', film);
                film.posterURL = Api.getPoster(film);
                if (film.ratingMPAA)
                    film.ratingMPAA = Api.mpaaDesc[film.ratingMPAA] || film.ratingMPAA;
                return film;
            });
    };


    Api.getPoster = function (film) {
        return 'http://st.kp.yandex.net/images/film_big/' + (film.id || film.filmID) + '.jpg';
    };

    return Api;
})

module.filter('filterByGenre', function () {
    return function (films, genre) {
        console.log('--- filtering by genre: ' + genre);
        if (genre.toUpperCase() === "ЛЮБОЙ")
            return films;
        return films.filter(function (f) {
            return f.genre.indexOf(genre) >= 0;
        });
    }
});


module.filter('sortByRating', function ($filter) {
    return function (films, enableSorting) {
        if (!enableSorting)
            return films;
        return $filter('orderBy')(films, 'rating').reverse();
    };
});
