app = angular.module('app', ['kinopoisk', 'yandex-maps']);

app.controller('settings', ['$rootScope', 'kpApi', function ($rootScope, kpApi) {

    $rootScope.api = kpApi;

    $rootScope.setOrder = function (orderByRating) {
        $rootScope.orderByRating = orderByRating;
    }


    kpApi.reloadFilms();

}]);


app.controller('films', ['$rootScope', '$scope', 'kpApi', function ($rootScope, $scope, kpApi) {
    $scope.getRatingPercent = function (rating) {
        return 100 - Math.round(+rating.split('(')[0] / 10 * 100);
    }
    $scope.setCurrentFilm = function (filmID, cityID) {
        kpApi.getFilm(filmID).then(function (film) {
            $rootScope.film = film;
        });
        kpApi.getSeances(filmID, cityID).then(function (seances) {
            console.log('--- sesanses for film: ', seances.items);
            $rootScope.currentSeances = seances.items;
        });
    }
}]);


app.controller('film', ['$scope', 'kpApi', function ($scope, kpApi) {
    $scope.setAddress = function (address) {
        $scope.currentAddress = kpApi.cities[kpApi.currentCity] + ',' + address;
        $('#yandex_map_wrapper').show();
    }
    $scope.hideMap = function () {
        $('#yandex_map_wrapper').hide();
    }
}]);
